import React from "react";
import { Doughnut } from "react-chartjs-2";

// import firebase from "../../config/client";
// import { useAuthState } from "react-firebase-hooks/auth";
// import Router from "next/router";

export default function DisChart({ likes, dislikes }) {
	// const [user, loading, error] = useAuthState(firebase.auth());

	// let nLikes = 0;
	// let nDislikes = 0;

	// const doc = firebase
	// 	.firestore()
	// 	.collection("users")
	// 	.doc(user.uid)
	// 	.get()
	// 	.then((snap) => {
	// 		nLikes = snap.data().liked1.length;
	// 		nDislikes = snap.data().disliked1.length;
	// 	});

	// const delay = 1000;
	// let timer;
	// const done = () => {
	// 	timer = setTimeout(async () => {
	// 		try {
	// 			firebase
	// 				.firestore()
	// 				.collection("users")
	// 				.doc(user.uid)
	// 				.get()
	// 				.then((snap) => {
	// 					nLikes = snap.data().liked1.length;
	// 					nDislikes = snap.data().disliked1.length;
	// 				});
	// 		} catch {
	// 			console.log(err);
	// 		}
	// 	}, delay);
	// };
	// Router.events.on("routeChangeComplete", done());

	const state = {
		labels: ["Liked", "Disliked"],
		datasets: [
			{
				label: "Data",
				backgroundColor: ["#FF6384", "#36A2EB"],
				data: [likes, dislikes],
				hoverOffset: 4,
			},
		],
	};

	return (
		<div>
			<Doughnut
				data={state}
				options={{
					plugins: {
						legend: {
							position: "bottom",
							display: true,
							labels: {
								color: "#ffffff",
								font: {
									size: 15,
								},
							},
						},
						title: {
							display: true,
							text: "Stage 1 - Discoverers",
							font: {
								size: 16,
							},
						},
					},
				}}
				width={400}
				height={400}
			/>
		</div>
	);
}
