import React from "react";
import { Doughnut } from "react-chartjs-2";

// import firebase from "../../config/client";
// import { useAuthState } from "react-firebase-hooks/auth";

export default function ExpChart({ likes, dislikes }) {
	// const [user, loading, error] = useAuthState(firebase.auth());

	// let nLikes = 0;
	// let nDislikes = 0;

	// const doc = firebase
	// 	.firestore()
	// 	.collection("users")
	// 	.doc(user.uid)
	// 	.get()
	// 	.then((snap) => {
	// 		nLikes = snap.data().liked2.length;
	// 		nDislikes = snap.data().disliked2.length;
	// 	});

	const state = {
		labels: ["Liked", "Disliked"],
		datasets: [
			{
				label: "Data",
				backgroundColor: ["#FF6384", "#bded39"],
				data: [likes, dislikes],
				hoverOffset: 4,
			},
		],
	};

	return (
		<div>
			<Doughnut
				data={state}
				options={{
					plugins: {
						legend: {
							position: "bottom",
							display: true,
							labels: {
								color: "#ffffff",
								font: {
									size: 15,
								},
							},
						},
						title: {
							display: true,
							text: "Stage 2 - Explorers",
							font: {
								size: 16,
							},
						},
					},
				}}
				width={400}
				height={400}
			/>
		</div>
	);
}
