import React from "react";
import styles from "../styles/Footer.module.css";
import {
	FaAt,
	FaEnvelope,
	FaFacebookF,
	FaGithub,
	FaGlobe,
	FaInstagram,
	FaLocationArrow,
	FaMapPin,
	FaPhone,
	FaPhoneSlash,
	FaSearchLocation,
	FaStreetView,
	FaTwitter,
} from "react-icons/fa";

const Footer = () => {
	return (
		<>
			<div className={styles.container}>
				<div className={styles.group}>
					<div className={styles.socialGrp}>
						<h1 className={styles.grpTitle}>Social Media</h1>
						<p>
							<a href="https://www.instagram.com/hellocareer.in/">
								{" "}
								<FaInstagram className={styles.ico} />
							</a>
							<FaFacebookF
								className={styles.ico}
								color="#4267B2"
							/>
							<FaTwitter className={styles.ico} color="#1DA1F2" />
							<FaGithub className={styles.ico} />
						</p>
					</div>
					<div className={styles.organGrp}>
						<h1 className={styles.grpTitle}>Organisation</h1>
						<p className={styles.oText}>
							<FaEnvelope />
							<b> Email: </b>support@hellocareer.co.in
						</p>
						<p className={styles.oText}>
							<FaPhone />
							<b> Phone: </b>+91 123456789
						</p>
						<p className={styles.oText}>
							<FaGlobe />
							<b> Website: </b>hellocareer.co.in
						</p>
					</div>
					<div className={styles.localGrp}>
						<h1 className={styles.grpTitle}>Location</h1>
						<p className={styles.oText}>
							<FaStreetView />
							<b> Street: </b> Nothing right now
						</p>
						<p className={styles.oText}>
							<FaMapPin />
							<b> City: </b>Bengaluru - 560050
						</p>
					</div>
				</div>
				<h3 className={styles.fText} style={{ textAlign: "center" }}>
					Copyrights @ HelloCareer 2021
				</h3>
			</div>
		</>
	);
};

export default Footer;
