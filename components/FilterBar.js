import React from "react";
import styles from "../styles/FilterBar.module.css";

const FilterBar = () => {
  return (
    <div>
      <div className={styles.filters}>
        <button className={styles.chBtn}>
          English Grammar<b>(10)</b>
        </button>
        <button className={styles.chBtn}>
          Hindi<b>(20)</b>
        </button>
        <button className={styles.chBtn}>
          Kannada<b>(30)</b>
        </button>
        <button className={styles.chBtn}>
          Physics<b>(40)</b>
        </button>
        <button className={styles.chBtn}>
          Chemistry<b>(50)</b>
        </button>
        <button className={styles.chBtn}>
          Mathematics<b>(50)</b>
        </button>
        <button className={styles.chBtn}>
          Biology<b>(60)</b>
        </button>
        <button className={styles.chBtn}>
          Computers<b>(60)</b>
        </button>
        <button className={styles.chBtn}>
          History Civics<b>(70)</b>
        </button>
        <button className={styles.chBtn}>
          Geography<b>(90)</b>
        </button>
        <button className={styles.chBtn}>
          English Literature<b>(1)</b>
        </button>
        <button className={styles.chBtn}>
          Defense<b>(100)</b>
        </button>
        <br />
      </div>
    </div>
  );
};

export default FilterBar;
