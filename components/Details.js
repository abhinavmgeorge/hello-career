import firebase from "../config/client";
import "firebase/firestore";

function Details({ pos }) {
	const readData = (id) => {
		id = pos;
		try {
			firebase
				.firestore()
				.collection("users")
				.doc(id)
				.onSnapshot((doc) => {
					console.log(doc.data());
				});
		} catch (error) {
			alert(error);
		}
	};

	return (
		<div>
			<button onClick={readData(pos)}>Read Data</button>
		</div>
	);
}

export default Details;
