import firebase from "../config/client";
import "firebase/firestore";

function Details(id) {
	const readData = (id) => {
		try {
			firebase
				.firestore()
				.collection("users")
				.doc(id)
				.onSnapshot((doc) => {
					console.log(doc.data());
				});
		} catch (error) {
			alert(error);
		}
	};

	return (
		<div>
			<button onClick={readData("data")}>Read Data</button>
		</div>
	);
}

export default Details;
