import Navbar from "../components/Navbar";
import styles from "../styles/Index.module.css";
import Footer from "../components/Footer";
import Image from "next/image";
import img1 from "../public/images/image1.png";
import img2 from "../public/images/image2.png";
import Link from "next/link";
import Head from "next/head";
import { useRouter } from "next/router";

export default function Home() {
	const router = useRouter();
	const getStart = () => {
		router.push("/login");
	};

	return (
		<>
			<Head>
				<meta
					name="google-site-verification"
					content="w5AEpKTK4j7WT2iTr-uW4B__Q29_Vxk27xneiA4XahY"
				/>
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0"
				/>
				<meta
					name="description"
					content="Find your passion and set your future"
				/>
				<meta property="og:title" content="HelloCareer | Home" />
				<meta
					property="og:description"
					content="Find your passion and set your future"
				/>
				<meta property="og:url" content="https://hellocareer.co.in" />
				<meta property="og:type" content="website" />

				<title>HelloCareer | Home</title>
			</Head>
			{/* The navbar */}
			<Navbar />

			<div className={styles.container}>
				<section>
					<div className={styles.row}>
						<div className={styles.leftS}>
							<h1 className={styles.mTitle}>Hello Career</h1>
							<p className={styles.sDesc}>
								Find your passion.
								<br />
								Work to become not acquire
							</p>
							<button className={styles.mBtn} onClick={getStart}>
								Get Started For Free
							</button>
						</div>
						<div className={styles.rightS}>
							<Image
								className={styles.imgCon}
								src={img1}
								alt="Image 1"
							/>
							<p className={styles.mDesc}>
								If you don’t love what you are doing, then you
								won’t be able to do it with passion and enjoy
								it. We will help you find your passion to and
								get you to know about what you should do in
								future.
							</p>
							<h4 className={styles.readMore}>
								Read More{" "}
								<Link href="/about">
									<a>About Us</a>
								</Link>
							</h4>
						</div>
					</div>
				</section>
				<section>
					<div className={styles.row}>
						<div className={styles.leftC}>
							<h1 className={styles.s2Title}>
								How Do We Help Students?
							</h1>
							<p className={styles.s2Desc}>
								If you are confused or don’t really know what
								you’re passionate about then you’ve come to the
								right place! We have fun ways to help you find
								your dream job and choose a career that you will
								not regret and will also help you to proceed in
								achieving your dream!
							</p>
						</div>
						<div className={styles.rightC}>
							<Image
								className={styles.imgCon}
								src={img2}
								alt="Image 2"
							/>
						</div>
					</div>
				</section>

				<section>
					<div className={styles.finalRow}>
						<div className={styles.dis}>
							<div className={styles.card}>
								<h1 className={styles.cTitle}>Discoverers</h1>
								<p className={styles.cDesc}>
									You Discover, While We Explore
								</p>
								<p className={styles.mCon}>
									The stage 1 @HelloCareer provides students
									with a vast variety of career options to
									choose from based on one’s interest.
									Different choices from different streams
									have been put forward with all the details
									about the job profile. He/She can simply
									choose a career that suits their personality
									and will be something that they can enjoy a
									blissful future.
								</p>
							</div>
						</div>
						<div className={styles.exp}>
							<div className={styles.card}>
								<h1 className={styles.cTitle}>Explorers</h1>
								<p className={styles.cDesc}>
									Now you can explore specifically while we
									take you one step closer to achievement
								</p>
								<p className={styles.mCon}>
									After going through a vast variety of
									options when you have finally narrowed down
									to your main focus , we will help you to
									know more about that specific profile all at
									one place. Every job profile you have right
									swiped in stage 1 has been in dept written
									in stage two . Here you will be exposed to
									every aspect of the selected job profile,
									from responsibilities to education, from
									maximum to minimum salary.
								</p>
							</div>
						</div>
						<div className={styles.ach}>
							<div className={styles.card}>
								<h1 className={styles.cTitle}>Achievers</h1>
								<p className={styles.cDesc}>
									The Fullstop To Your Dream Decision
								</p>
								<p className={styles.mCon}>
									Lastly @HelloCareer we help you connect to
									people who have work experience of more than
									3 years in multiple professions to help you
									lock-in the career options that you have
									short listed. This is the last step of your
									ladder which is an assurance for your career
									option. These people will answer every doubt
									of yours and help you to finalise your
									option or help you correct any mistakes that
									you have made on your way up till here.
								</p>
							</div>
						</div>
					</div>
				</section>
			</div>
			<Footer />
		</>
	);
}
