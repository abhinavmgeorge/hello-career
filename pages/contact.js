import Navbar from "../components/Navbar"
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Contact.module.css'
import { FaAt, FaEnvelope, FaFacebookF, FaGithub, FaGlobe, FaInbox, FaInstagram, FaPaperPlane, FaTwitter, FaUser } from 'react-icons/fa'
import contImg from '../public/images/contactUs.jpeg'

const Contact = () => {
    return (
        <>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>HelloCareer | Contact Us</title>
            </Head>

            <Navbar />

            <div className={styles.container}>
                <h1 className={styles.title}>Contact Us</h1>
                <div className={styles.row}>
                    <div className={styles.column}>
                        <Image className={styles.img} src={contImg} alt="Contact Page Image" layout="responsive" />
                    </div>
                    <div className={styles.rightSide}>
                        <h2 className={styles.sideH}>WRITE TO US</h2>
                        <form>
                            <FaUser style={{ color: '#e05100' }} />
                            <input className={styles.field} type="text" placeholder="Full Name" name="name" id="name" required />
                            <br />
                            <FaEnvelope style={{ color: '#e05100' }} />
                            <input className={styles.field} type="email" placeholder="Email" name="email" id="email" required />
                            <br />
                            <FaInbox style={{ color: '#e05100' }} />
                            <input className={styles.field} type="text" placeholder="Subject" name="sub" id="sub" required />
                            <br />
                            <textarea className={styles.field} placeholder="Your message to us" name="messageInfo" id="msg" cols="30" rows="5"></textarea>
                            <br />
                            <button className={styles.sendBtn} type="submit">
                                <b>SEND MESSAGE </b><FaPaperPlane />
                            </button>
                        </form>
                        <hr style={{ width: '75%', marginTop: '-12px' }} />
                        <div className={styles.socialBottom}>
                            <h3 className={styles.joinMsg}>Or Reach Us Socially?</h3>
                            <div className={styles.socialIcons}>
                                <FaInstagram className={styles.bIcon} />
                                <FaFacebookF className={styles.bIcon} style={{ color: '#4267B2' }} />
                                <FaTwitter className={styles.bIcon} style={{ color: '#1DA1F2' }} />
                                <FaAt className={styles.bIcon} />
                                <FaGithub className={styles.bIcon} />
                                <FaGlobe className={styles.bIcon} style={{ color: '#92D293' }} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Contact
