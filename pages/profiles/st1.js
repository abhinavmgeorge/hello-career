import React from "react";
import { FaTimesCircle, FaCheckCircle } from "react-icons/fa";
import styles from "../../styles/CardBox.module.css";

import firebase from "../../config/client";
import { useAuthState } from "react-firebase-hooks/auth";
import Head from "next/head";

export default function Stage1({ data }) {
	const db = firebase.firestore().collection("users");
	const [mUser, mLoading, mError] = useAuthState(firebase.auth());

	async function addToDB(mTitle, kind) {
		if (kind === "like") {
			await db.doc(mUser.uid).update({
				liked1: firebase.firestore.FieldValue.arrayUnion(mTitle),
			});
		} else {
			await db.doc(mUser.uid).update({
				disliked1: firebase.firestore.FieldValue.arrayUnion(mTitle),
			});
		}
	}

	let id = Math.floor(Math.random() * data.length + 1);
	let mTitle = "";

	async function changeData(kind) {
		try {
			if (kind === "like") {
				document.getElementById("mTitle").innerHTML = data[id].title;
				mTitle = document.getElementById("mTitle").innerHTML;
				document.getElementById("mDesc").innerHTML = data[id].desc;
			}

			if (kind === "dislike") {
				document.getElementById("mTitle").innerHTML = data[id].title;
				mTitle = document.getElementById("mTitle").innerHTML;
				document.getElementById("mDesc").innerHTML = data[id].desc;
			}
			id++;

			addToDB(mTitle, kind);
		} catch {
			document.getElementById("mTitle").innerHTML = "Error Occured";
			document.getElementById("mDesc").innerHTML =
				"Please refresh the browser... A client side error has occured";
		}
	}

	return (
		<div>
			<Head>
				<title>HelloCareer | Stage 1</title>
			</Head>
			{/* <FilterBar /> */}

			<div className={styles.bottomBody}>
				<div className={styles.cardContainer}>
					{/* <a href="#dislike"><i className="opt fa fa-times-circle"></i></a> */}
					<FaTimesCircle
						onClick={() => changeData("dislike")}
						className={[styles.options, styles.cross_opt].join(" ")}
					/>
					<div className={styles.card}>
						<h1 className={styles.title} id="mTitle">
							{data[id].title}
						</h1>
						<p className={styles.cardAbt} id="mDesc">
							{data[id].desc}
						</p>
					</div>
					<FaCheckCircle
						onClick={() => changeData("like")}
						className={[styles.options, styles.check_opt].join(" ")}
					/>
					{/* <a href="#like"><i className="opt fa fa-check-circle"></i></a> */}
				</div>
			</div>
		</div>
	);
}

export async function getServerSideProps({ res }) {
	try {
		const result = await fetch("https://hcdbapi.herokuapp.com/s1/all");
		// const result = await fetch("https://hcdbapi.herokuapp.com/s1/all");
		const data = await result.json();

		return {
			props: { data },
		};
	} catch {
		res.statusCode = 404;
		return {
			props: {},
		};
	}
}
