import React from "react";
import styles from "../../styles/S2.module.css";
import { FaTimesCircle, FaCheckCircle } from "react-icons/fa";

import firebase from "../../config/client";
import { useAuthState } from "react-firebase-hooks/auth";
import Head from "next/head";

export default function Stage2({ data }) {
	const db = firebase.firestore().collection("users");
	const [user, loading, error] = useAuthState(firebase.auth());

	async function addToDB(mTitle, kind) {
		if (kind === "like") {
			await db.doc(user.uid).update({
				liked2: firebase.firestore.FieldValue.arrayUnion(mTitle),
			});
		} else {
			await db.doc(user.uid).update({
				disliked2: firebase.firestore.FieldValue.arrayUnion(mTitle),
			});
		}
	}

	let id = Math.floor(Math.random() * data.length + 1);
	let mTitle = "";

	async function changeData(kind) {
		try {
			if (kind === "like") {
				document.getElementById("mTitle").innerHTML = data[id].title;
				mTitle = document.getElementById("mTitle").innerHTML;
				document.getElementById("content").innerHTML = data[id].resp;
			}

			if (kind === "dislike") {
				document.getElementById("mTitle").innerHTML = data[id].title;
				mTitle = document.getElementById("mTitle").innerHTML;
				document.getElementById("content").innerHTML = data[id].resp;
			}
			id++;

			addToDB(mTitle, kind);
		} catch {
			document.getElementById("mTitle").innerHTML = "Error Occured";
			document.getElementById("mCon").innerHTML =
				"Please refresh the browser... A client side error has occured";
		}
	}

	function qualBtn() {
		if (document.getElementById("qBtn").innerHTML === "Responsibility") {
			document.getElementById("content").innerHTML = data[id].resp;
			document.getElementById("qBtn").innerHTML = "Qualification";
		} else {
			document.getElementById("content").innerHTML = data[id].qual;
			document.getElementById("qBtn").innerHTML = "Responsibility";
		}
	}

	function salBtn() {
		if (document.getElementById("sBtn").innerHTML === "Responsibility") {
			document.getElementById("content").innerHTML = data[id].resp;
			document.getElementById("sBtn").innerHTML = "Salary";
		} else {
			document.getElementById("content").innerHTML = data[id].sal;
			document.getElementById("sBtn").innerHTML = "Responsibility";
		}
	}

	return (
		<div>
			<Head>
				<title>HelloCareer | Stage 2</title>
			</Head>
			<div className={styles.container}>
				<div className={styles.region}>
					<FaTimesCircle
						onClick={() => changeData("dislike")}
						className={[styles.options, styles.cross_opt].join(" ")}
					/>
					<div className={styles.card}>
						<h1 className={styles.title} id="mTitle">
							{data[id].title}
							{/* ASTROPHYSICIST */}
						</h1>
						<div className={styles.cardAbt} id="mCon">
							<div className={styles.content}>
								<p id="content">{data[id].resp}</p>
							</div>
							<div className={styles.bottomOpt}>
								<button
									onClick={() => qualBtn()}
									id="qBtn"
									className={styles.chBtn}
								>
									Qualification
								</button>
								<button
									onClick={() => salBtn()}
									id="sBtn"
									className={styles.chBtn}
								>
									Salary
								</button>
							</div>
						</div>
					</div>
					<FaCheckCircle
						onClick={() => changeData("like")}
						className={[styles.options, styles.check_opt].join(" ")}
					/>
				</div>
			</div>
		</div>
	);
}

export async function getServerSideProps({ res }) {
	try {
		const result = await fetch("https://hcdbapi.herokuapp.com/s2/all");
		const data = await result.json();

		return {
			props: { data },
		};
	} catch {
		res.status = 404;
		return {
			props: {},
		};
	}
}
