import Head from "next/head";
import React from "react";
import styles from "../../styles/ProIndex.module.css";
import { useRouter } from "next/router";
import { useEffect } from "react";

function ProfilesIndex() {
	const router = useRouter();

	function goto(url) {
		router.push(url);
	}

	return (
		<div>
			<Head>
				<title>HelloCareer | Profiles</title>
			</Head>
			<div className={styles.container}>
				<div
					onClick={() => goto("/profiles/st1")}
					className={[styles.s1Card, styles.card].join(" ")}
				>
					<div className={styles.inner}>
						<h1>Discoverers</h1>
						<p>(Stage 1)</p>
					</div>
				</div>
				<div
					onClick={() => goto("/profiles/st2")}
					className={[styles.s2Card, styles.card].join(" ")}
				>
					<div className={styles.inner}>
						<h1>Exploreres</h1>
						<p>(Stage 2)</p>
					</div>
				</div>
			</div>
		</div>
	);
}

export default ProfilesIndex;
