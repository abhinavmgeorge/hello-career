import React from "react";
import styles from "../styles/Filter.module.css";

const Filter = () => {
  return (
    <div>
      <div className={styles.container}>
        <div className={styles.card}>
          <h1 className={styles.title}>English Grammar</h1>
          <p className={styles.val}>(10)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>Hindi</h1>
          <p className={styles.val}>(100)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>Kannada</h1>
          <p className={styles.val}>(1230)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>Physics</h1>
          <p className={styles.val}>(120)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>Chemistry</h1>
          <p className={styles.val}>(1230)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>Mathematics</h1>
          <p className={styles.val}>(1420)</p>
        </div>

        <div className={styles.card}>
          <h1 className={styles.title}>Biology</h1>
          <p className={styles.val}>(1012)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>Computers</h1>
          <p className={styles.val}>(4310)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>History</h1>
          <p className={styles.val}>(1230)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>Geography</h1>
          <p className={styles.val}>(1540)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>English Literature</h1>
          <p className={styles.val}>(2310)</p>
        </div>
        <div className={styles.card}>
          <h1 className={styles.title}>Defense</h1>
          <p className={styles.val}>(12530)</p>
        </div>
      </div>
    </div>
  );
};

export default Filter;
