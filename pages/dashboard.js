import React, { useEffect } from "react";
import Head from "next/head";
import Navbar from "../components/Navbar";
import styles from "../styles/Dashboard.module.css";
import { FaQuestionCircle, FaSignOutAlt } from "react-icons/fa";
import { IoMdSettings } from "react-icons/io";
import Link from "next/link";
import toggleSettingsMenu from "../scripts/dashScript";
import { Router, useRouter } from "next/router";

import firebase from "../config/client";
import { useAuthState } from "react-firebase-hooks/auth";
import { useCollection } from "react-firebase-hooks/firestore";
import ExpChart from "../components/charts/ExpChart";
import DisChart from "../components/charts/DisChart";
// import Details from "../components/Details";

export default function Dashboard() {
	const db = firebase.firestore().collection("users");
	const router = useRouter();

	const [mUser, mLoading, mError] = useAuthState(firebase.auth());

	function getStarted() {
		router.push("/profiles");
	}

	const delay = 900;
	let timer;
	let nl1 = 10;
	let nd1 = 8;
	let nl2 = 8;
	let nd2 = 17;
	const done = () => {
		timer = setTimeout(async () => {
			await db.doc(mUser.uid).set(
				{
					username: mUser.displayName,
					email: mUser.email,
					uid: mUser.uid,
					pPic: mUser.photoURL,
				},
				{ merge: true }
			);

			// once done get the data
			try {
				db.doc(mUser.uid)
					.get()
					.then((content) => {
						document.getElementById("unameField").innerHTML =
							content.data().username;
						document.getElementById(
							"profileImg"
						).style.backgroundImage = "url(content.data().pPic)";
						nl1 = content.data().liked1.length;
						nd1 = content.data().disliked1.length;
						nl2 = content.data().liked2.length;
						nd2 = content.data().disliked2.length;
						console.log(nl1, nl2, nd1, nd2);
						document.getElementById("li").innerHTML =
							"Likes: " + nl1;
						document.getElementById("dl").innerHTML =
							"Dislikes: " + nd1;
						document.getElementById("li2").innerHTML =
							"Likes: " + nl2;
						document.getElementById("dl2").innerHTML =
							"Dislikes: " + nd2;
					});
			} catch (err) {
				console.log(err);
			}
		}, delay);
	};
	Router.events.on("routeChangeComplete", done());

	return (
		<div>
			<Head>
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0"
				/>
				<title>HelloCareer | Dashboard</title>
			</Head>
			<div>
				<Navbar />

				{mLoading && <h4>Loading...</h4>}
				{mUser && (
					<div className={styles.mainCon}>
						<div className={styles.sidebar}>
							{/* <h2>Username: {user.displayName}</h2> */}
							<h2 id="unameField">Username</h2>
							<h1 id="nice"></h1>
							<div
								id="profileImg"
								className={styles.profilePic}
								style={
									{
										// backgroundImage: "url(user.photoURL.toString())"
									}
								}
							></div>
							<div className={styles.opt}>
								<Link href="#">
									<a onClick={toggleSettingsMenu}>
										<IoMdSettings
											className={styles.oIcon}
										/>
									</a>
								</Link>
								<FaQuestionCircle className={styles.oIcon} />
								<FaSignOutAlt className={styles.oIcon} />
							</div>

							<div className={styles.settingSet} id="settingOpt">
								<h3 className={styles.sTitle}>Switch Theme</h3>
								<h3 className={styles.sTitle}>
									Password Change
								</h3>
								<h3 className={styles.sTitle}>
									Phone Number Change
								</h3>
								<h3 className={styles.sTitle}>
									Change Profile Picture
								</h3>
							</div>

							<div className={styles.buttonSide}>
								<button
									id="btnStart"
									onClick={() => getStarted()}
									className={styles.getStart}
								>
									Get Started ➜{" "}
								</button>
							</div>
						</div>
						<div className={styles.main}>
							<h1>My Account</h1>
							<div className={styles.container}>
								<div className={styles.dis} id="disCard">
									<DisChart likes={12} dislikes={5} />
									<div className={styles.bOpts}>
										<p id="li">Likes: </p>
										<p id="dl">Dislikes: </p>
									</div>
								</div>
								<div className={styles.exp} id="expCard">
									<ExpChart likes={10} dislikes={17} />
									<div className={styles.bOpts}>
										<p id="li2">Likes: </p>
										<p id="dl2">Dislikes: </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		</div>
	);
}
