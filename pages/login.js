import React from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import Navbar from "../components/Navbar";
import Image from "next/image";
import styles from "../styles/Login.module.css";
import pageImage from "../public/images/login2.jpg";
import Head from "next/head";
import {
	FaArrowRight,
	FaEnvelope,
	FaFacebookF,
	FaGithub,
	FaGooglePlusG,
	FaInstagram,
	FaLock,
	FaTwitter,
} from "react-icons/fa";
import firebase from "../config/client";

const Login = () => {
	const uiConfig = {
		signInSuccessUrl: "/dashboard",
		signInOptions: [
			firebase.auth.GithubAuthProvider.PROVIDER_ID,
			firebase.auth.EmailAuthProvider.PROVIDER_ID,
			firebase.auth.GoogleAuthProvider.PROVIDER_ID,
		],
	};

	// const [user, loading, error] = useAuthState(firebase.auth());
	// console.log('loading', loading, '|', 'User', user)

	return (
		<>
			<Head>
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0"
				/>
				<meta
					name="description"
					content="Create or login to an existing account to get the full out of the website"
				/>
				<meta property="og:title" content="HelloCareer | Login" />
				<meta
					property="og:description"
					content="Create or login to an existing account to get the full out of the website"
				/>
				<meta
					property="og:url"
					content="https://hellocareer.co.in/login"
				/>
				<meta property="og:type" content="website" />
				<title>HelloCareer | Login</title>
			</Head>
			<Navbar />

			<div className={styles.container}>
				<div className={styles.row}>
					<div>
						<Image
							className={styles.pImage}
							src={pageImage}
							alt="Login page image"
						/>
					</div>
					<div className={styles.rightSide}>
						<h1 className={styles.pTitle}>Welcome Back Achiever</h1>
						{/* <form>
                            <FaEnvelope style={{ color: '#e05100' }} />
                            <input className={styles.field} type="email" placeholder="Email" name="email" id="email" required />
                            <br />
                            <FaLock style={{ color: '#e05100' }} />
                            <input className={styles.field} type="password" placeholder="Password" name="pass" id="pass" required />
                            <br />
                            <button className={styles.logBtn} type="submit"><b>LOGIN </b><FaArrowRight /></button>
                        </form>
                        <a className={styles.joinMsg} href="#signUpPage">Or Why Not Create A Free Account?</a>
                        <hr style={{ width: '75%' }} />
                        <div>
                            <h3 className={styles.joinMsg}>Or Login Socially?</h3>
                            <div className={styles.socialIcons}>
                                <FaInstagram className={styles.ico} />
                                <FaFacebookF className={styles.ico} style={{ color: '#4267B2' }} />
                                <FaTwitter className={styles.ico} style={{ color: '#1DA1F2' }} />
                                <FaGithub className={styles.ico} />
                                <FaGooglePlusG className={styles.ico} style={{ color: '#e05' }} />
                            </div>
                        </div> */}
						<StyledFirebaseAuth
							uiConfig={uiConfig}
							firebaseAuth={firebase.auth()}
						/>
					</div>
				</div>
			</div>
		</>
	);
};

export default Login;
